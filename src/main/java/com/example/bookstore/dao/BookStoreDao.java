package com.example.bookstore.dao;

import com.example.bookstore.model.Author;
import com.example.bookstore.model.Book;
import com.example.bookstore.repository.AuthorRepository;
import com.example.bookstore.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class BookStoreDao {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public String addBook(String bookName, double price, String authorName)
    {
        String status = "SUCCESS";
        try {

            Book book = bookRepository.getAllByTitle(bookName);
            Author author = authorRepository.getAllByName(authorName);
            if(author == null){
                author = new Author();
                author.setName(authorName);
            }
            if(book == null) {
                book = new Book();
                book.setId(null);
                book.setTitle(bookName);
                book.setAuthor(author);
                book.setPrice(price);
                book.setNumOfCopies(1);
            }
            else{
                int count = book.getNumOfCopies();
                count++;
                book.setNumOfCopies(count);
                book.setTitle(bookName);
                book.setAuthor(author);
                book.setPrice(price);
            }
            bookRepository.save(book);
        }
        catch (Exception e){
            logger.log(Level.INFO, e.getMessage());
            status = "FAILURE";
        }
        return status;
    }
    public Book buyBook(String bookName)
    {
        String status = "SUCCESS";
        Book book = null;
        try{
            book = bookRepository.getAllByTitle(bookName);
            int count = book.getNumOfCopies();
            count--;
            if(count == 0)
                count = 1;
            book.setNumOfCopies(count);
            bookRepository.save(book);
        }
        catch(Exception e){
            logger.log(Level.INFO, e.getMessage());
            status = "FAILURE";
        }
        if(status.equals("SUCCESS"))
            return book;
        return null;
    }
    public String addAuthor(String authorName)
    {
        String status = "SUCCESS";
        try{
            Author author = new Author();
            author.setId(null);
            author.setName(authorName);
            authorRepository.save(author);
        }
        catch (Exception e){
            logger.log(Level.INFO, e.getMessage());
            status = "FAILURE";
        }
        return status;
    }
    public Book getByISBN(Integer bookISBN)
    {
        Optional<Book> book = bookRepository.findById(bookISBN);
        if(book.isPresent())
            return book.get();
        return null;
    }
    public Book getByTitle(String title)
    {
        return bookRepository.getAllByTitle(title);
    }
    public ArrayList<Book> getByAuthor(String authorName)
    {
        return bookRepository.getAllByAuthorName(authorName);
    }
    public ArrayList<Book> getByPrice(double price)
    {
        return bookRepository.getAllByPrice(price);
    }
}
