package com.example.bookstore.repository;

import com.example.bookstore.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Book getAllByTitle(String title);
    ArrayList <Book> getAllByAuthorName(String authorName);
    ArrayList <Book> getAllByPrice (double price);

}
