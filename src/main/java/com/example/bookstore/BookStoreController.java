package com.example.bookstore;

import com.example.bookstore.dao.BookStoreDao;
import com.example.bookstore.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class BookStoreController {

    @Autowired
    private BookStoreDao bookStoreDao;
    private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/addBook/{bookName}/{price}/{authorName}", method=GET, produces = {"application/json"})
    @ResponseBody
    public String addBook(@PathVariable("bookName") String bookName, @PathVariable("price") String price, @PathVariable("authorName") String authorName)
    {
        double value = Double.parseDouble(price);
        return bookStoreDao.addBook(bookName, value, authorName);
    }
    @RequestMapping(value = "/addAuthor/{authorName}", method=GET, produces = {"application/json"})
    @ResponseBody
    public String addAuthor(@PathVariable("authorName") String authorName)
    {
        return bookStoreDao.addAuthor(authorName);
    }

    @GetMapping("/searchByAuthor")
    @ResponseBody
    public ArrayList<Book> getByAuthor(String authorName)
    {
        return bookStoreDao.getByAuthor(authorName);
    }
    @GetMapping("/searchByPrice")
    @ResponseBody
    public ArrayList<Book> getByPrice(double price)
    {
        return bookStoreDao.getByPrice(price);
    }
    @GetMapping("/searchByISBN")
    @ResponseBody
    public Book getByISBN(int bookISBN) {
        Book book = bookStoreDao.getByISBN(bookISBN);
        if(book == null)
            logger.log(Level.INFO, "No Books found");
        return book;

    }
    @GetMapping("/searchByTitle")
    @ResponseBody
    public Book getByTitle(String title){
        Book book = bookStoreDao.getByTitle(title);
        if(book == null)
            logger.log(Level.INFO, "No Books found by this Title");
        return book;
    }

    @GetMapping("/getMediaCoverage")
    @ResponseBody
    public ArrayList<String> mediaCoverage(String bookName)
    {
        ArrayList<String> result = new ArrayList<>();
        ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity("https://jsonplaceholder.typicode.com/posts", Object[].class);
        Object[] resp = responseEntity.getBody();

        for(int i = 0; i < resp.length; i++)
        {
            Map<String, Object> map = (Map<String, Object>) resp[i];
            String value1 = (String) map.get("title");
            String value2 = (String) map.get("body");
            if(value1.contains(bookName) || value2.contains(bookName))
                result.add(value1);
        }
        return result;
    }
    @GetMapping("/buyBook")
    @ResponseBody
    public Book buyBook(String bookName)
    {
        Book book = bookStoreDao.buyBook(bookName);
        if(book == null)
            logger.log(Level.INFO, "This book is not available to buy");
        return book;
    }
}
