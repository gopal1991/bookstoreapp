package com.example.bookstore;

import com.example.bookstore.dao.BookStoreDao;
import com.example.bookstore.model.Author;
import com.example.bookstore.model.Book;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookStoreControllerTest {

    @Mock
    BookStoreDao bookStoreDao;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    private BookStoreController bookStoreController;

    @Before
    public void setUp()
    {
        bookStoreDao = Mockito.mock(BookStoreDao.class);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddBook()
    {
        when(bookStoreController.addBook("Title", "100.0", "Author")).thenReturn("SUCCESS");
        assertEquals(bookStoreController.addBook("Title", "100.0", "Author"), "SUCCESS");
    }
    @Test
    public void testGetByPrice()
    {
        Book book = new Book();
        ArrayList<Book> list = new ArrayList<>();
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(new Author());
        list.add(book);
        when(bookStoreController.getByPrice(Mockito.anyDouble())).thenReturn(list);
        Assert.assertEquals(bookStoreController.getByPrice(100), list);
    }

    @Test
    public void testGetByAuthor()
    {
        Book book = new Book();
        Author author = new Author();
        author.setName("Author1");
        ArrayList<Book> list = new ArrayList<>();
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(author);
        list.add(book);
        when(bookStoreController.getByAuthor(Mockito.anyString())).thenReturn(list);
        Assert.assertEquals(bookStoreController.getByAuthor("Author1"), list);
    }
    @Test
    public void testGetByISBN()
    {
        Book book = new Book();
        Author author = new Author();
        author.setName("Author1");
        ArrayList<Book> list = new ArrayList<>();
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(author);
        list.add(book);
        when(bookStoreController.getByISBN(Mockito.anyInt())).thenReturn(book);
        Assert.assertEquals(bookStoreController.getByISBN(1), book);
    }
    @Test
    public void testAddAuthor()
    {
        Mockito.when(bookStoreController.addAuthor(Mockito.anyString())).thenReturn("SUCCESS");
        Assert.assertEquals(bookStoreController.addAuthor("Author2"), "SUCCESS");
    }
    @Test
    public void testGetMediaCoverage()
    {
        Map<String, Object> map = new HashMap();
        Object[] object = new Object[1];
        map.put("userId", 1);
        map.put("id", 2);
        map.put("title", "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
        map.put("body", "quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto");
        object[0] = map;

        when(restTemplate.getForEntity("https://jsonplaceholder.typicode.com/posts", Object[].class)).thenReturn(new ResponseEntity<>(object, HttpStatus.OK));
        ArrayList<String> result = bookStoreController.mediaCoverage("book");
        Assert.assertEquals(result, new ArrayList<>());
    }

}
