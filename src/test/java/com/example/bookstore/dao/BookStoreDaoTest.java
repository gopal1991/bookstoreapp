package com.example.bookstore.dao;

import com.example.bookstore.BookStoreController;
import com.example.bookstore.model.Author;
import com.example.bookstore.model.Book;
import com.example.bookstore.repository.AuthorRepository;
import com.example.bookstore.repository.BookRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class BookStoreDaoTest {
    @Mock
    BookRepository bookRepository;

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    BookStoreDao bookStoreDao;

    @Test
    public void testAddBook()
    {
        Book book = new Book();
        Author author = new Author();
        author.setName("Author");
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(author);
        Mockito.when(bookRepository.getAllByTitle(Mockito.anyString())).thenReturn(book);
        Mockito.when(authorRepository.getAllByName(Mockito.anyString())).thenReturn(author);
        Mockito.when(bookRepository.save(book)).thenReturn(book);
        Assert.assertEquals(bookStoreDao.addBook("Title", 100, "Author"), "SUCCESS");
    }
    @Test
    public void testAddBookNullCondition()
    {
        Book book = new Book();
        Mockito.when(bookRepository.getAllByTitle(Mockito.anyString())).thenReturn(null);
        Mockito.when(authorRepository.getAllByName(Mockito.anyString())).thenReturn(null);
        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(book);
        Assert.assertEquals(bookStoreDao.addBook("Title", 100, "Author"), "SUCCESS");
    }
    @Test
    public void testBuyBook()
    {
        Book book = new Book();
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(new Author());
        book.setNumOfCopies(1);

        Mockito.when(bookRepository.getAllByTitle(Mockito.anyString())).thenReturn(book);
        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(book);
        Assert.assertEquals(bookStoreDao.buyBook("Title"), book);
    }
    @Test
    public void testAddAuthor()
    {
        Mockito.when(authorRepository.save(Mockito.any())).thenReturn(null);
        Assert.assertEquals(bookStoreDao.addAuthor("Author"), "SUCCESS");
    }
    @Test
    public void testGetByISBN()
    {
        Book book = new Book();
        Author author = new Author();
        author.setName("Author");
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(author);
        book.setId(1);

        Mockito.when(bookRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(book));
        Assert.assertEquals(bookStoreDao.getByISBN(Mockito.anyInt()), book);
    }
    @Test
    public void testGetByTitle()
    {
        Book book = new Book();
        Author author = new Author();
        author.setName("Author");
        book.setId(1);
        book.setTitle("Title");
        book.setPrice(100);
        book.setAuthor(author);
        book.setId(1);
        Mockito.when(bookRepository.getAllByTitle(Mockito.anyString())).thenReturn(book);
        Assert.assertEquals(bookStoreDao.getByTitle("Title"), book);
    }
}
